# integration

## Jenkins Integration

* Jenkinsfile - the Jenkins Pipeline script
* scripts/ValidateScorecard.groovy - the Groovy script to validate the provided git repository URL

Make sure the following plugins are installed:
* Pipeline
* Git

### Install HTTP Request plugin

To install `HTTP Request` plugin go to
Manage Jenkins -> Manage Plugins -> Available
and find `HTTP Request`
click on the checkbox -> Install without restart

### Create Pipeline in Jenkinks

New Item -> Pipeline -> OK

Configure it as follows:

Pipeline
    Definition: Pipeline script from SCM
    SCM: Git
    Repository URL: https://gitlab.com/assessment-scorecard/integration


Press `Save` button

### Test build

Create some scorecards in the back-end with all questions answered, any question is not answered. 
Click on `Build Now` after some time it will pause until you provide a git repository URL.
